package fr.cea.irfu;

import javax.swing.JFrame;

import org.jdesktop.swingx.JXTable;

public class Application {

	public static void main(String[] args) {
		System.out.println("Hello Workd");
		
		JFrame frame =  new JFrame("Mon appli");
		
		//headers for the table
        String[] columns = new String[] {
            "Id", "Name", "Hourly Rate", "Part Time"
        };
         
        //actual data for the table in a 2d array
        Object[][] data = new Object[][] {
            {1, "John", 40.0, false },
            {2, "Rambo", 70.0, false },
            {3, "Zorro", 60.0, true },
        };
        
		JXTable maTable = new JXTable(data,columns );
		frame.setContentPane(maTable);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 300);
		frame.setVisible(true);
		
	}
}
